<?php
/**
 * The template for displaying all single posts.
 */

get_header(); ?>

<div id="content" class="site-content container">
	<div class="row">
		<div id="primary" class="content-area col-md-9 col-sm-9 col-xs-12">
			<main id="main" class="site-main" role="main">
				<h1 class="page-title title text-center"><?php esc_html_e( '404. Page not found!', 'ladita' ); ?></h1>
				<img src="<?php echo get_template_directory_uri() ?>/img/404-thiet-ke-web.gif" alt="404-error-page-not-found">

				<p><?php esc_html_e('It seem like the content you look is not exist', 'ladita') ?></p>

				<p><?php esc_html_e('Try to search in website:', 'ladita') ?></p>

				<?php get_search_form() ?>

				<p><?php esc_html_e('Or return to', 'ladita') ?> <a href="<?php echo home_url('/') ?>"><?php esc_html_e('Home page') ?></a></p>

			</main>
		</div>
		<div class="col-md-3 col-sm-3 col-xs-12">
			<?php get_sidebar() ?>
		</div>
	</div>
</div>

<?php get_footer(); ?>