<?php
/**
 * The template for displaying the homepage
 * Template name: Homepage
 */
get_header();
?>

<?php if (have_posts()): while (have_posts()): the_post(); ?>

    <div id="content" class="site-content container">
        <div class="row">
            <div id="primary" class="content-area col-md-9 col-sm-9 col-xs-12">
                <main id="main" class="site-main" role="main">
                    <?php onter_top_news() ?>
                    <?php onter_top_slide_news() ?>

                    <?php onter_ads_banner('http://localhost/petpet.vn/wp-content/uploads/2019/06/banner-cat.jpg', '#', 'Cat banner ads') ?>

                    <div id="home-categories" class="section">
                        <div class="row">
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <?php onter_top_news_incategory(1) ?>
                            </div>
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <?php onter_top_news_incategory(3) ?>
                            </div>
                        </div>
                    </div>
                    <?php onter_ads_banner('http://localhost/petpet.vn/wp-content/uploads/2019/06/dogs-banner.jpg', '#', 'Dog banner ads') ?>

                    <?php onter_list_posts_enable_load_more() ?>

                </main>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-12">
                <?php get_sidebar() ?>
            </div>
        </div>
    </div>

<?php endwhile; endif; ?>

<?php get_footer(); ?>