<?php

if(true && is_customize_preview()) {

    // Include Kirki
    if ( ! class_exists( 'Kirki' ) ) {
        include_once( dirname( __FILE__ ) . '/kirki/kirki.php' );
    }

    if ( ! function_exists( 'onter_kirki_update_url' ) ) {
        function onter_kirki_update_url( $config ) {
            $config['url_path'] = get_template_directory_uri() . '/inc/customizer/kirki/';
            return $config;
        }
    }
    add_filter( 'kirki/config', 'onter_kirki_update_url' );

    // Theme options
    include_once dirname(__FILE__) . '/options/option_typography.php';
}