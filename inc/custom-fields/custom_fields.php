﻿<?php

/** Include ACF Plugin */
// Define path and URL to the ACF plugin.
define( 'ONTER_ACF_PATH', dirname(__FILE__) . '/includes/advanced-custom-fields/' );
define( 'ONTER_ACF_URL', dirname(__FILE__) . '/includes/advanced-custom-fields/' );

// Include the ACF plugin
include_once( ONTER_ACF_PATH . 'acf.php' );

// Include ACF Repeater
include_once( dirname(__FILE__) . '/includes/acf-repeater/acf-repeater.php' );

// Customize the url setting to fix incorrect asset URLs
add_filter('acf/settings/url', 'onter_acf_settings_url');
function onter_acf_settings_url( $url ) {
    return ONTER_ACF_URL;
}

// Remove all visual interfaces
define( 'ACF_LITE', true );

// (Optional) Hide the ACF admin menu item
add_filter('acf/settings/show_admin', 'onter_acf_settings_show_admin');
function onter_acf_settings_show_admin( $show_admin ) {
    return false;
}

/** Include your custom fields */

// post type book
include_once( dirname(__FILE__) . '/acf/acf_post_type_book.php' );

// taxonomy book category
include_once( dirname(__FILE__) . '/acf/acf_taxonomy_book_category.php' );