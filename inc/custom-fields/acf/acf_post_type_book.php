<?php

if(function_exists("register_field_group"))
{
    register_field_group(array (
        'id' => 'acf_test-post-field',
        'title' => 'Test post field',
        'fields' => array (
            array (
                'key' => 'field_5d70adb66d7e3',
                'label' => 'Price',
                'name' => 'price',
                'type' => 'repeater',
                'sub_fields' => array (
                    array (
                        'key' => 'field_5d70addd6d7e4',
                        'label' => 'Number',
                        'name' => 'number',
                        'type' => 'number',
                        'column_width' => '',
                        'default_value' => '',
                        'placeholder' => '',
                        'prepend' => '',
                        'append' => '',
                        'min' => '',
                        'max' => '',
                        'step' => '',
                    ),
                    array (
                        'key' => 'field_5d70adf46d7e5',
                        'label' => 'Value',
                        'name' => 'value',
                        'type' => 'number',
                        'column_width' => '',
                        'default_value' => '',
                        'placeholder' => '',
                        'prepend' => '',
                        'append' => '',
                        'min' => '',
                        'max' => '',
                        'step' => '',
                    ),
                ),
                'row_min' => 2,
                'row_limit' => 10,
                'layout' => 'row',
                'button_label' => 'Add Price',
            ),
        ),
        'location' => array (
            array (
                array (
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'post',
                    'order_no' => 0,
                    'group_no' => 0,
                ),
            ),
        ),
        'options' => array (
            'position' => 'normal',
            'layout' => 'no_box',
            'hide_on_screen' => array (
            ),
        ),
        'menu_order' => 0,
    ));
}