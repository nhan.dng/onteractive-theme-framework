<?php

/**
 * Create custom taxonomy and custom post type
 */

add_action('init', 'onter_create_custom_taxonomy_book_category');
function onter_create_custom_taxonomy_book_category() {
    register_taxonomy('book_category', 'book', array(
        'labels' => array(
            'name' => __('Book Categories', 'onter'),
            'singular' => __('Book Category', 'onter'),
            'menu_name' => __('Book Categories', 'onter')
        ),
        'hierarchical' => true,
        'public' => true,
        'show_ui' => true,
        'show_admin_column' => true,
        'show_in_nav_menus' => true,
        'show_tagcloud' => true,
        'rewrite' => array(
            'slug' => 'book-category'
        ),
    ));
}

add_action('init', 'onter_create_custom_post_type_book');
function onter_create_custom_post_type_book() {
    register_post_type('book', array(
        'labels' => array(
            'name' => __('Books', 'onter'),
            'singular_name' => __('Book', 'onter')
        ),
        'description' => __('Books', 'onter'),
        'supports' => array(
            'title', 'editor', 'thumbnail'
        ),

        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => true,
        'show_in_admin_bar' => true,
        'menu_position' => 7,
        'menu_icon' => 'dashicons-images-alt2',
        'can_export' => true,
        'has_archive' => true,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'capability_type' => 'post',
        'rewrite'            => array(
            'slug' => 'book'
        ),
    ));
}