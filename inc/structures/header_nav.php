<?php

// Header banner
function onter_header_banner() {
    dynamic_sidebar('sidebar-header-banner');
}

/**
 * Display post thumbnail
 */
function onter_post_thumbnail($size = 'thumbnail', $args = array()) {
    if (has_post_thumbnail() && !post_password_required() || has_post_format('image')): ?>
        <a class="post-thumbnail" href="<?php the_permalink() ?>" title="<?php the_title_attribute() ?>" rel="bookmark">
            <figure><?php the_post_thumbnail($size, $args); ?></figure>
        </a>
    <?php endif;
}

/**
 * Display post title
 */
function onter_post_title() {
    ?>
    <h3 class="entry-title" itemprop="name">
        <a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
            <?php the_title(); ?>
        </a>
    </h3>
    <?php
}

/**
 * Display post excerpt
 */
function onter_post_excerpt() {
    ?>
    <div class="entry-summary"><?php the_excerpt() ?></div>
    <?php
}

/**
 * Display post meta
 */
function onter_post_meta() {
    ?>
    <div class="entry-meta">
        <?php echo onter_get_post_view() ?>
        | <span itemprop="datePublished"><?php the_time('m/d/Y') ?></span>
        | <span class="list-categories"><?php echo get_the_category_list(', ') ?></span>
    </div>
    <?php
}

/**
 * Social share buttons
 */
function onter_social_share_buttons() {
    $title = get_the_title();
    $link = get_permalink();
    ?>

    <ul class="social-share list-inline clearfix">
        <li>
            <a style="background: #4460a0" onclick="window.open('https://www.facebook.com/sharer/sharer.php?u=<?php echo $link ?>','Share', 'toolbar=0,status=0,width=620,height=280');"
               data-toggle="tooltip" title="" href="javascript:" data-original-title="Share on Facebook"> <i
                    class="fa fa-facebook"></i> </a></li>
        <li>
            <a style="background: #24a9e6" onclick="popUp=window.open('http://twitter.com/home?status=<?php echo $title ?> <?php echo $link ?>','sharer','scrollbars=yes,width=800,height=400');popUp.focus();return false;"
               data-toggle="tooltip" title="" href="javascript:;" data-original-title="Share on Twitter"> <i
                    class="fa fa-twitter"></i> </a></li>
        <li><a style="background: #f24033" data-toggle="tooltip" title="" href="javascript:;"
               onclick="popUp=window.open('https://plus.google.com/share?url=<?php echo $link ?>','sharer','scrollbars=yes,width=800,height=400');popUp.focus();return false;"
               data-original-title="Share on Google +"> <i class="fa fa-google-plus"></i> </a></li>
        <li><a style="background: #187fb8" data-toggle="tooltip" title=""
               onclick="popUp=window.open('http://linkedin.com/shareArticle?mini=true&amp;url=<?php echo $link ?>&amp;title=<?php echo $title ?>','sharer','scrollbars=yes,width=800,height=400');popUp.focus();return false;"
               href="javascript:;" data-original-title="Share on Linkedin"> <i class="fa fa-linkedin"></i> </a></li>
        <li><a style="background: #32455c" data-toggle="tooltip" title=""
               onclick="popUp=window.open('http://www.tumblr.com/share/link?url=<?php echo $link ?>&amp;name=<?php echo $title ?>&amp;description=<?php the_excerpt() ?>','sharer','scrollbars=yes,width=800,height=400');popUp.focus();return false;"
               href="javascript:;" data-original-title="Share on Tumblr"> <i class="fa fa-tumblr"></i> </a></li>
        <li><a style="background: #c6232c" data-toggle="tooltip" title=""
               onclick="popUp=window.open('http://pinterest.com/pin/create/button/?url=<?php echo $link ?>&amp;description=<?php echo $title ?>&amp;media=<?php the_post_thumbnail_url('full') ?>','sharer','scrollbars=yes,width=800,height=400');popUp.focus();return false;"
               href="javascript:;" data-original-title="Share Pinterest"> <i class="fa fa-pinterest"></i> </a>
        </li>
    </ul>

    <?php
}

/**
 * Plugin Comment Facebook
 */
function onter_facebook_comment($data_width = '') {
    if (!$data_width) $data_width = 850;
    ?>

    <div class="facebook-comment responsive">
        <h4 class="title">BÌNH LUẬN CỦA BẠN</h4>
        <div class="fb-comments" data-href="<?php the_permalink() ?>" data-width="<?php echo $data_width ?>px" data-numposts="25"></div>
    </div>

    <?php
}

/**
 * Show Related Posts
 */
function onter_related_posts()
{
    $id = get_the_ID();
    $terms = wp_get_post_tags($id);

    if ($terms) {
        $term_ids = array();
        foreach ($terms as $item) $term_ids[] = $item->term_id;
        $args = array(
            'tag__in' => $term_ids,
            'post__not_in' => array($id),
            'posts_per_page' => 8,
            'ignore_sticky_posts' => 1
        );
        query_posts($args);
        if (have_posts()) {
            while (have_posts()) {
                the_post();
                get_template_part('template-parts/content/content', 'list');
            }
        }
        wp_reset_query();
    }
}

function onter_get_the_excerpt($limit) {
    $excerpt = explode(' ', get_the_excerpt(), $limit);
    if (count($excerpt) >= $limit) {
        array_pop($excerpt);
        $excerpt = implode(" ", $excerpt).' ...';
    } else {
        $excerpt = implode(" ",$excerpt);
    }
//    $excerpt = preg_replace('`\[[^\]]*\]`','',$excerpt);
    return $excerpt;
}

function onter_the_excerpt($limit) {
    echo onter_get_the_excerpt($limit);
}

function onter_breadcrumbs() {
    ?>
    <?php if ( function_exists('yoast_breadcrumb') )
    {yoast_breadcrumb('<div id="breadcrumbs">','</div>');} ?>
<?php
}

function onter_promo_posts($index = 0) {
    $sticky = get_option('sticky_posts');
    // check if there are any
    if (!empty($sticky)) {
        // optional: sort the newest IDs first
        rsort($sticky);
        // override the query
        $args = array(
            'post__in'          => $sticky,
            'post_type'         => 'post',
            'posts_per_page'    => 5,
            'offset'            => $index*5,
            'ignore_sticky_posts' => 1,
        );
        query_posts($args);
        ?>
        <div role="tabpanel" class="tab-pane fade<?php echo $index==0?' active in':'' ?>" id="promo<?php echo $index ?>">
            <?php
            if (have_posts()) {
                while (have_posts()) {
                    the_post();
                    get_template_part('template-parts/content/content', '2');
                }
            }
            ?>
        </div>
        <?php wp_reset_query();
    }
}

function onter_top_news() {
    ?>
    <div id="top-news" class="section">
        <?php
        query_posts(array(
            'ignore_sticky_posts' => 1,
            'post_type' => 'post',
            'posts_per_page' => 3
        ));
        ?>
        <div class="row">
            <div class="col-md-8 col-sm-8 col-xs-12">
                <?php
                if (have_posts()) {
                    the_post();
                    get_template_part('template-parts/content/content');
                }
                ?>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12">
                <?php
                if (have_posts()) {
                    while (have_posts()) {
                        the_post();
                        get_template_part('template-parts/content/content', '2');
                    }
                }
                wp_reset_query();
                ?>
            </div>
        </div>
    </div>
    <?php
}

function onter_top_slide_news() {
    ?>
    <div id="top-slide-news" class="section">
        <div class="control prev"><i class="fa fa-angle-left"></i></div>
        <div class="control next"><i class="fa fa-angle-right"></i></div>
        <div class="news-carousel" id="top-slide-news-carousel">
            <?php
            query_posts(array(
                'ignore_sticky_posts' => 1,
                'post_type' => 'post',
                'posts_per_page' => 6,
                'offset' => 3
            ));
            if (have_posts()) {
                while (have_posts()) {
                    the_post();
                    get_template_part('template-parts/content/content', '2');
                }
            }
            wp_reset_query();
            ?>
        </div>
    </div>
    <?php
}

function onter_top_news_incategory($cat_id = 1) {
    $cat_name = get_cat_name($cat_id);
    ?>
    <h3 class="onter-title"><?php echo $cat_name ?></h3>
    <?php
    query_posts(array(
        'ignore_sticky_posts' => 1,
        'post_type' => 'post',
        'cat' => $cat_id,
        'posts_per_page' => 5
    ));
    if (have_posts()) {
        the_post();
        get_template_part('template-parts/content/content', '2');

        echo '<ul class="list-posts">';
        while (have_posts()) {
            the_post();
            get_template_part('template-parts/content/content', 'list');
        }
        echo '</ul>';
    }
    wp_reset_query();
    ?>
    <?php
}

// list posts - load more when scroll down
function onter_list_posts_enable_load_more() {
    ?>
    <div id="home-news" class="section">
        <?php
        query_posts(array(
            'post_type' => 'post',
            'ignore_sticky_posts' => 1,
            'posts_per_page' => 20,
            'offset' => 2
        ));
        if (have_posts()) {
            while (have_posts()) {
                the_post();
                get_template_part('template-parts/content/content', 'aside');
            }
        }
        wp_reset_query();
        ?>
    </div>
    <div id="home-news-loader">
        <div class="loader"><div></div><div></div><div></div>
    </div>
    <?php
}

function onter_pagination() {
    global $wp_query;
    if ( $wp_query->max_num_pages > 1 ) : ?>
        <div class="clearfix">
            <ul class="archive-nav clearfix">
                <?php
                if ( get_previous_posts_link() ) echo '<li class="archive-nav-newer">' . get_previous_posts_link( '&larr; ' . __('Previous', 'rowling')) . '</li>';
                $paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
                $max   = intval( $wp_query->max_num_pages );
                /**	Add current page to the array */
                if ( $paged >= 1 )
                    $links[] = $paged;
                /**	Add the pages around the current page to the array */
                if ( $paged >= 3 ) {
                    $links[] = $paged - 1;
                    $links[] = $paged - 2;
                }
                if ( ( $paged + 2 ) <= $max ) {
                    $links[] = $paged + 2;
                    $links[] = $paged + 1;
                }
                /**	Link to first page, plus ellipses if necessary */
                if ( ! in_array( 1, $links ) ) {
                    $class = 1 == $paged ? ' active' : '';
                    printf( '<li class="number%s"><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1' );
                    if ( ! in_array( 2, $links ) ) echo '<li>...</li>';
                }
                /**	Link to current page, plus 2 pages in either direction if necessary */
                sort( $links );
                foreach ( (array) $links as $link ) {
                    $class = $paged == $link ? ' active' : '';
                    printf( '<li class="number%s"><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
                }
                /**	Link to last page, plus ellipses if necessary */
                if ( ! in_array( $max, $links ) ) {
                    if ( ! in_array( $max - 1, $links ) ) echo '<li class="number">...</li>' . "\n";
                    $class = $paged == $max ? ' active' : '';
                    printf( '<li class="number%s"><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );
                }
                if ( get_next_posts_link() ) echo '<li class="archive-nav-older">' . get_next_posts_link( __('Next', 'rowling') . ' &rarr;') . '</li>';
                ?>
            </ul>
        </div>
    <?php endif;
}

function onter_ads_banner($img_src, $link, $title) {
    ?>
    <div class="ads-banner">
        <a href="<?php echo $link ?>" target="_blank" rel="nofollow" title="<?php echo $title ?>"><img src="<?php echo $img_src ?>" alt="<?php echo $title ?>"></a>
    </div>
    <?php
}