<?php

/** Set up theme */
require get_template_directory() . '/inc/functions/setup.php';

/** Custom WP Admin */
require get_template_directory() . '/inc/functions/custom_wp_admin.php';

/** Functions */
require get_template_directory() . '/inc/functions/functions.php';

/** Custom post type and taxonomy */
require get_template_directory() . '/inc/post-types/post_type_book.php';

/** Custom fields */
require get_template_directory() . '/inc/custom-fields/custom_fields.php';

/** Customizer */
require get_template_directory() . '/inc/customizer/customizer.php';

/** Shortcodes */
require get_template_directory() . '/inc/shortcodes/shortcode_list_posts.php';

/** Structures */
require get_template_directory() . '/inc/structures/header_nav.php';

/** Widgets */
require get_template_directory() . '/inc/widgets/widget_latest_posts.php';

/** Page builder */
//require get_template_directory() . '/inc/page-builder/shortcode_abc.php';

/** Woocommerce */
//require get_template_directory() . '/inc/woocommerce/woo_abc.php';