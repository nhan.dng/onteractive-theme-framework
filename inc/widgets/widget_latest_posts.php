<?php

/**
 * Register widgets
 */
add_action('widgets_init', 'onter_register_widgets');

function onter_register_widgets()
{
    register_widget('onter_Promo_Posts_Widget');
}

/**
 * Class onter_Promo_Posts_Widget
 */

class onter_Promo_Posts_Widget extends WP_Widget {

    function __construct() {
        /* Widget settings. */
        $widget_ops = array( 'classname' => 'onter-promo-posts-widget', 'description' => 'Promo Posts Widget' );
        /* Widget control settings. */
        $control_ops = array( 'id_base' => 'onter-promo-posts-widget' );
        /* Create the widget. */
        parent::__construct('onter-promo-posts-widget', 'onter Promo Posts Widget', $widget_ops, $control_ops);
    }

    function form( $instance ) {
        $default = array(
            'title' => 'Tin tài trợ'
        );
        $instance = wp_parse_args( (array) $instance, $default );
        $title = esc_attr($instance['title']);
        ?>
        <p>Title:</p>
        <p><input type="text" class="widefat" name="<?php echo $this->get_field_name('title') ?>" value="<?php echo $title ?>"></p>
        <?php
    }

    function update( $new_instance, $old_instance ) {
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        return $instance;
    }

    function widget( $args, $instance ) {
        extract($args);
        $title = apply_filters( 'widget_title', $instance['title'] );
        echo $before_widget; ?>
        <h3 class="widget-title"><?php echo $title ?></h3>
        <div class="widget-content">
            <div class="tab-content">
                <?php for ($i = 0; $i < 3; $i++) onter_promo_posts($i); ?>
            </div>
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#promo0" aria-controls="promo0" role="tab" data-toggle="tab">1</a></li>
                <li role="presentation"><a href="#promo1" aria-controls="promo1" role="tab" data-toggle="tab">2</a></li>
                <li role="presentation"><a href="#promo2" aria-controls="promo2" role="tab" data-toggle="tab">3</a></li>
            </ul>
        </div>
        <?php echo $after_widget;
    }
}
// end class