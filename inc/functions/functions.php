<?php

// ajax load more project
add_action('wp_ajax_nopriv_load_more_posts', 'onter_load_more_posts');
add_action('wp_ajax_load_more_posts', 'onter_load_more_posts');

function onter_load_more_posts() {
    $post_offset = $_POST['post_offset'];
    query_posts(array(
        'post_type'             => 'post',
        'offset'                => $post_offset,
        'posts_per_page'        => 5,
        'ignore_sticky_posts'   => 1
    ));

    if (have_posts()) {
        while (have_posts()) {
            the_post();
            get_template_part('template-parts/content/content', 'aside');
        }
    }
    wp_reset_query();
    die();
}

// add post view as post meta
function onter_posts_column_views( $columns ) {
    $columns['post_views'] = 'Views';
    return $columns;
}
function onter_posts_custom_column_views( $column ) {
    if ( $column === 'post_views') {
        echo onter_get_post_view();
    }
}

add_filter( 'manage_posts_columns', 'onter_posts_column_views' );
add_action( 'manage_posts_custom_column', 'onter_posts_custom_column_views' );

function onter_get_post_view() {
    $count = get_post_meta( get_the_ID(), 'post_views_count', true );
    return '<i class="fa fa-eye"></i> ' . ($count?$count:'1');
}
function onter_set_post_view() {
    $key = 'post_views_count';
    $post_id = get_the_ID();
    $count = (int) get_post_meta( $post_id, $key, true );
    $count++;
    update_post_meta( $post_id, $key, $count );
}