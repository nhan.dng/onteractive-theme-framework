<?php

$theme_version = wp_get_theme()->get('Version');

add_action( 'after_setup_theme', 'onter_setup_theme' );
function onter_setup_theme() {

    /**
     * Make theme available for translation
     */
    load_theme_textdomain( 'onter', get_template_directory() . '/languages' );

    // Add theme support for selective refresh for widgets.
    add_theme_support( 'customize-selective-refresh-widgets' );

    // Add support for Block Styles.
    add_theme_support( 'wp-block-styles' );

    // Add support for full and wide align images.
    add_theme_support( 'align-wide' );

    // Add support for editor styles.
    add_theme_support( 'editor-styles' );

    // Add support for responsive embedded content.
    add_theme_support( 'responsive-embeds' );

    add_theme_support( 'widget-customizer' );
    add_theme_support( 'menus' );

    /**
     * Add default posts and comments RSS feed links to head.
     */
    add_theme_support('automatic-feed-links');

    // Declare support for title theme feature
    add_theme_support('title-tag');

    // Enable support for Post Thumbnails on posts and pages.
    add_theme_support('post-thumbnails');

    // Add support for core custom logo
    add_theme_support(
        'custom-logo',
        array(
            'flex-width'  => true,
            'flex-height' => true,
        )
    );

    // Declare WooCommerce support
    add_theme_support('woocommerce');

    // Enqueue editor styles in admin
    add_editor_style( 'assets/css/admin_editor_style.css' );

    /*
     * Switch default core markup for search form, comment form, comments, galleries, captions and widgets
     * to output valid HTML5.
     */
    add_theme_support(
        'html5',
        array(
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption',
            'widgets',
        )
    );

    // Create menus
    register_nav_menus(array(
        'main-nav' => __('Main Nav', 'onter'),
        'top-bar-nav' => __('Top bar Nav', 'onter'),
    ));

    set_post_thumbnail_size(560, 345, true);
    //add_image_size( 'small', 60, 45, true );

    // set content width
    //$GLOBALS['content_width'] = 800;

}

// Register widget areas
add_action('widgets_init', 'onter_widgets_init');
function onter_widgets_init() {
    register_sidebar(array(
        'name' => __('Main Sidebar', 'onter'),
        'id' => 'sidebar-main',
        'description' => '',
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ));

    register_sidebar(array(
        'name' => __('Footer Sidebar 1', 'onter'),
        'id' => 'sidebar-footer-1',
        'description' => '',
        'before_widget' => '<section id="%1$s" class="widget col-md-3 %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h4 class="footer-title">',
        'after_title' => '</h4>',
    ));

    register_sidebar(array(
        'name' => __('Footer Sidebar 2', 'onter'),
        'id' => 'sidebar-footer-2',
        'description' => '',
        'before_widget' => '<section id="%1$s" class="widget col-md-3 %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h4 class="footer-title">',
        'after_title' => '</h4>',
    ));
}

// enqueue styles
add_action( 'wp_enqueue_scripts', 'onter_enqueue_styles', 10 );
function onter_enqueue_styles() {
    global $theme_version;

    wp_enqueue_style('onter-style', get_stylesheet_uri(), array(), $theme_version);
    wp_enqueue_style('owl-carousel-style', get_template_directory_uri() . '/assets/css/owl.carousel.min.css', array(), $theme_version);
    wp_enqueue_style('owl-carousel-theme-style', get_template_directory_uri() . '/assets/css/owl.theme.default.min.css', array(), $theme_version);
    wp_enqueue_style('custom-style', get_template_directory_uri() . '/assets/css/custom.css', array(), $theme_version);
    wp_enqueue_style('font-awesome-style', get_template_directory_uri() . '/assets/css/font-awesome.min.css');
}

// enqueue scripts
add_action( 'wp_enqueue_scripts', 'onter_enqueue_scripts', 10 );
function onter_enqueue_scripts() {
    global $theme_version;

    wp_deregister_script('jquery');
    wp_enqueue_script('jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js', array(), null, true);
    wp_enqueue_script('bootstrap-js', get_template_directory_uri() . '/assets/js/bootstrap.min.js', array(), $theme_version, true);
    wp_enqueue_script('owl-carousel-js', get_template_directory_uri() . '/assets/js/owl.carousel.min.js', array(), $theme_version, true);
    wp_enqueue_script('main-js', get_template_directory_uri() . '/assets/js/main.js', array(), $theme_version, true);

    // register ajax object
    wp_localize_script('main-js', 'ajax_object', array(
        'url' => admin_url( 'admin-ajax.php' )
    ));
}

/**
 * Change excerpt more to ...
 */
add_filter('excerpt_more', 'onter_excerpt_more');
function onter_excerpt_more( $more ) {
    return ' ...';
}

/**
 * Add async attribute to script
 */
add_filter( 'script_loader_tag', 'onter_add_async_attribute', 10, 2 );
function onter_add_async_attribute($tag, $handle) {
    // add script handles to the array below
    $scripts_to_async = array('bootstrap-js', 'main-js');

    foreach ($scripts_to_async as $script) {
        if ($script === $handle) {
            return str_replace(' src', ' async="async" src', $tag);
        }
    }
    return $tag;
}

/**
 * Fix skip link focus in IE11
 */
add_action( 'wp_print_footer_scripts', 'onter_skip_link_focus_fix' );
function onter_skip_link_focus_fix() {
    ?>
    <script>
        /(trident|msie)/i.test(navigator.userAgent)&&document.getElementById&&window.addEventListener&&window.addEventListener("hashchange",function(){var t,e=location.hash.substring(1);/^[A-z0-9_-]+$/.test(e)&&(t=document.getElementById(e))&&(/^(?:a|select|input|button|textarea)$/i.test(t.tagName)||(t.tabIndex=-1),t.focus())},!1);
    </script>
    <?php
}