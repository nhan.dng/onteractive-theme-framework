<form class="searchform" method="get" action="<?php echo home_url('/') ?>">
    <input type="text" name="s" class="form-control" id="searchinput" placeholder="Tìm kiếm">
    <input type="hidden" name="post_type" value="post">
    <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
</form>