# Onteractive Framework
Wordpress theme framework for Onteractive Developers
## 1. Cấu trúc theme
### 1.1. assets/:
* **bootstrap/**: các file của bootstrap scss. Trong file bootstrap.scss có thể comment để remove những components không cần thiết.
* **scss/**: thư mục scss của dev
  * File chính là **custom.scss**, file này import file bootstrap ở trên và các file scss khác của dev. Overwrite các biến của bootstrap ở file **scss/_variables.scss** (*không edit trực tiếp trong file bootstrap/_variables.scss vì sẽ khó maintain code*)
    **File _utilities**: các hàm, class tiện ích sử dụng nhiều
    **File _header, _footer, _body**: css cho header, footer, body. Dev css cho các single, page, category ... nên tạo file riêng để tránh conflict.
* **css/**: các file css được build từ scss và các css plugin, thư viện khác. Scss được build thành file custom.css (build compress file để giảm kích thước)
* **fonts/**: fontawesome
* **img/**: ảnh
* **js/**: file javascript

### 1.2. inc/:
File functions.php của theme chỉ include file inc/init.php. Trong file này chúng ta sẽ include các file cần thiết. Thư mục inc/ gồm như thư mục con:
* **custom-fields/**: include plugin advanced custom field. Tính năng tạo custom fields trên admin được ẩn, thay vào đó sẽ dùng code PHP để tạo custom field (tránh được việc user tự ý vào edit dẫn đến lỗi, và việc đồng bộ code giữa các dev dễ dàng hơn).
  Mỗi custom fields group nên dev vào 1 file riêng trong thư mục acf/. Ví dụ có 1 post type là "book" cần thêm các custom fields, dev riêng vào file **acf_post_type_book.php**. Include file này vào cuối file inc/custom-fields/custom_fields.php.
  *Tips:* dùng admin tạo custom fields sau đó export thành code PHP.
  Xem thêm Document [Ở đây](https://www.advancedcustomfields.com/resources/).

* **customizer/**: theme options (logo, banner, background, color ...)
  Sử dụng framework Kirki.
  Dev các option vào thư mục **options/**.
  Xem thêm Document [Ở đây](https://kirki.org/docs/).

* **functions/**: chứa các file chức năng khác: set up theme, ajax, logic ...
  * File **setup.php**: được dev sẵn các set up theme.
  * File **custom_wp_admin.php**: khóa tính năng update core/theme/plugin, hide các menu không cần thiết đối với khách hàng.
  * File **functions.php**: các tính năng, logic khác. Khi dev nên tạo file riêng cho các tính năng để hạn chế conflict.

* **page-builder/**: code thêm các component cho page builder. Có thể gọi shortcode từ thư mục **shortcodes/**
* **post-types/**: tạo thêm các post type hoặc custom taxonomy
* **shortcodes/**: tạo thêm các shortcode
* **structures/**: các function structure cần thiết sử dụng nhiều nên viết tách riêng ra đây
* **widgets/**: code thêm các widget khác
* **woocommerce/**: cho dự án có sử dụng woocommerce

### 1.3. languages/: dịch theme

### 1.4. template-parts/:
* **content/**
* **header/**
* **footer/**

### 1.5. woocommerce: thư mục ghi đè template của woocommerce

## 2. Một số lưu ý / quy ước chung khi dev theme:
* Đặt tên thư mục dùng dấu -
* Tên file dùng dấu _ (trừ 1 số file template dùng dấu -)
* Tiền tố cho các biến, function: onter_
* Text domain: onter
* Tạo post type bằng code, không dùng plugin
* Tạo custom field bằng code, không dùng plugin
* Tách file dev riêng để hạn chế conflict
* Code khoa học, gọn gàng sáng sủa, dễ đọc, dễ hiểu, dễ bảo trì
* Đặt tên biến, function theo tiếng Anh, có ý nghĩa
* Luôn ghi chú chức năng của hàm

## 3. Security tips:
* Khi set up wordpress không để tiền tố bảng mặc định là `wp_`. Để thành `_on`.
* Không đặt username là admin. Đặt là onter_admin.
* Đặt mật khẩu đủ mạnh. Có thể đặt chung là Pass@Onteractive.
* Cấp cho khách hàng tài khoản admin riêng, tài khoản gốc dev vẫn giữ. Admin cho khách hàng đặt theo projectname_admin, pass như trên.
* Chmod file & thư mục theo khuyến cáo: https://thachpham.com/wordpress/wordpress-tutorials/cach-chmod-an-toan.html
* Luôn tắt bug khi public sản phẩm (trong file `wp-config.php`)
* Cài đặt plugin Ithemes security và cấu hình bảo mật.
* Không để đường dẫn admin mặc định. Đổi thành /onteradmin.

## 4. Các plugin khuyến cáo cho dự án:
* Visual composer: page builder
* Contact form 7
* Yoast SEO: hỗ trợ SEO, index website, sitemap ...
* Ithemes security: bảo mật
* Wp super cache (cho website đơn giản upload trên shared hosting) hoặc Total Cache (cho website phức tạp hơn, có quyền can thiệp server/vps).
* WP smtp: hỗ trợ gửi mail.
* WPML: đa ngôn ngữ.

## 5. Visual composer:
[Document](https://visualcomposer.com/features/developers/)