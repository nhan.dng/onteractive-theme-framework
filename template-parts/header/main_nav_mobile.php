<nav id="main-nav-mobile">
    <div id="mobile-menu-toggle"><i class="fa fa-reorder"></i></div>
    <div id="mobile-menu-wrapper">
        <?php
        wp_nav_menu(array(
            'theme_location' => 'main-nav'
        ));
        ?>
    </div>
    <div id="mobile-searchform">
        <?php get_search_form() ?>
    </div>
</nav>