<nav id="main-nav">
    <div class="container">
        <div class="pull-left">

            <?php
            wp_nav_menu(array(
                'theme_location' => 'main-nav'
            ));
            ?>

        </div>
        <div class="pull-right searchform-wrap">
            <div id="show-searchform"><i class="fa fa-search"></i></div>
            <div id="searchform-popup">

                <?php get_search_form() ?>

            </div>
        </div>
    </div>
</nav>