<header id="masthead" class="site-header" role="banner">
    <div class="header-wrapper">

        <?php get_template_part('template-parts/header/header_top') ?>
        <?php get_template_part('template-parts/header/header_main') ?>
        <?php get_template_part('template-parts/header/header_bottom') ?>

    </div>
</header>