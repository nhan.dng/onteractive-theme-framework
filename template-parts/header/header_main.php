<div id="header-main">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-4 col-md-4">
                <div class="site-logo">

                    <?php if (is_front_page()): ?>
                        <h1 class="site-title">
                            <a href="<?php echo esc_url(home_url()) ?>" rel="home" title="<?php bloginfo('name') ?>">
                                <?php bloginfo('name') ?>
                            </a>
                        </h1>
                    <?php endif; ?>

                    <?php the_custom_logo() ?>

                </div>
            </div>
            <div class="col-xs-12 col-sm-8 col-md-8">
                <div class="header-banner">

                </div>
            </div>
        </div>
    </div>
</div>