<?php
/**
 * Default Content
 */
?>
<article class="article" role="article" itemscope="" itemprop="blogPost" itemtype="http://schema.org/BlogPosting">
	<?php onter_post_thumbnail('post-thumbnail') ?>
	<?php onter_post_title() ?>
	<?php onter_post_excerpt() ?>
</article>