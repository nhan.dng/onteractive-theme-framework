<?php
/**
 * Content For Page
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> role="article" itemscope="" itemprop="blogPost" itemtype="http://schema.org/BlogPosting">
	<h1 class="entry-title" itemprop="name">
		<?php the_title() ?>
	</h1>
	<div class="entry-content clearfix">
		<?php the_content() ?>
	</div>
</article><!-- #post-## -->