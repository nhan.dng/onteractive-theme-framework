<?php
/**
 * Content For Single Post
 */
onter_set_post_view();
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> role="article" itemscope="" itemprop="blogPost" itemtype="http://schema.org/BlogPosting">
	<h1 class="entry-title" itemprop="name">
		<?php the_title() ?>
	</h1>
	<div class="clearfix meta-social">
		<div class="pull-left">
			<?php onter_post_meta() ?>
		</div>
		<div class="pull-right">
			<?php onter_social_share_buttons() ?>
		</div>
	</div>
	<?php onter_post_excerpt() ?>
	<ul class="related-posts">
		<?php onter_related_posts() ?>
	</ul>
	<div class="entry-content clearfix">
		<?php the_content() ?>
	</div>

	<div class="tags clearfix">
		<?php echo get_the_tag_list('<i class="fa fa-tags"></i> Tags: ', ', ', '') ?>
	</div>
</article>
