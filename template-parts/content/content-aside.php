<?php
/**
 * Default Content
 */
?>
<article class="article-aside clearfix" role="article" itemscope="" itemprop="blogPost" itemtype="http://schema.org/BlogPosting">
	<?php onter_post_thumbnail('post-thumbnail') ?>
	<div class="aside">
		<?php onter_post_title() ?>
		<?php onter_post_meta()?>
		<?php onter_post_excerpt() ?>
	</div>
</article>