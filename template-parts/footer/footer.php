<footer id="footer">
    <div class="footer-1 container">
        <div class="row">
            <?php dynamic_sidebar('sidebar-footer-1') ?>
        </div>
    </div>
    <div class="footer-2 container">
        <div class="row">
            <?php dynamic_sidebar('sidebar-footer-2') ?>
        </div>
    </div>
    <div class="copyright container">
        <div class="row">
            <div class="col-md-4">
                <p>© Copyright PetPet.vn. All right reserved.</p>
            </div>
            <div class="col-md-4 text-center">
                <div class="dmca">

                </div>
            </div>
            <div class="col-md-4 text-right">
                <p>Developed by Onteractive</p>
            </div>
        </div>
    </div>
</footer>