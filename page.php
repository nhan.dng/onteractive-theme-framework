<?php
/**
 * The template for displaying all pages.
 */

get_header(); ?>

<div id="content" class="site-content container">
		<div class="row">
			<div id="primary" class="content-area col-md-9 col-sm-9 col-xs-12">
				<main id="main" class="site-main" role="main">
					<div id="article" class="section">

					</div>
				</main>
			</div>
			<div class="col-md-3 col-sm-3 col-xs-12">

				<?php get_sidebar() ?>

			</div>
		</div>
	</div>

<?php get_footer(); ?>