<?php
/**
 * The template for displaying the footer.
 */
?>

<?php get_template_part('template-parts/footer/footer') ?>

<div id="up-to-top">
    <i class="fa fa-angle-up"></i>
</div>

<?php wp_footer(); ?>

</body>
</html>
