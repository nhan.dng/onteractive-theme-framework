<?php
/**
 * The sidebar containing the left widget area.
 */
if (!is_active_sidebar('sidebar-main')) return;
?>

<aside id="secondary" class="sidebar widget-area" role="complementary">
    <?php dynamic_sidebar( 'sidebar-main' ); ?>
</aside>