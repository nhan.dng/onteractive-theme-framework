<?php
/**
 * The template for displaying all single posts.
 */

get_header(); ?>

<div id="content" class="site-content container">
	<div class="row">
		<div id="primary" class="content-area col-md-9 col-sm-9 col-xs-12">
			<main id="main" class="site-main" role="main">
				<div id="article" class="section">
					<?php
					onter_breadcrumbs();
					while (have_posts()) {
						the_post();
						get_template_part('template-parts/content/content', 'single');
					}
					onter_facebook_comment();
					?>
				</div>
				<div id="in-category-posts" class="section">
					<h4 class="ladita-title">CÙNG CHUYÊN MỤC</h4>
					<div class="row">
						<?php
						$categories = get_the_category();

						if (!empty($categories)) {
							$cat_array = array();
							foreach ($categories as $cat) {
								array_push($cat_array, $cat->term_id);
							}
							query_posts(array(
								'ignore_sticky_posts' => 1,
								'post_type' => 'post',
								'category__in' => $cat_array,
								'posts_per_page' => 9
							));
							if (have_posts()) {
								while (have_posts()) {
									the_post();
									echo '<div class="col-md-4 col-sm-2 col-xs-12">';
									get_template_part('template-parts/content/content', '2');
									echo '</div>';
								}
							}
							wp_reset_query();
						}
						?>
					</div>
				</div>
				<h4 class="ladita-title">TIN NỔI BẬT</h4>

				<?php onter_top_news() ?>
				<?php onter_top_slide_news() ?>

				<div class="section">
					<div class="row">
						<div class="col-md-6 col-sm-12 col-xs-12">
							<?php onter_top_news_incategory(1) ?>
						</div>
						<div class="col-md-6 col-sm-12 col-xs-12">
							<?php onter_top_news_incategory(3) ?>
						</div>
					</div>
				</div>

				<?php onter_list_posts_enable_load_more() ?>

			</main>
		</div>
		<div class="col-md-3 col-sm-3 col-xs-12">
			<?php get_sidebar() ?>
		</div>
	</div>
</div>

<?php get_footer(); ?>