<?php
/**
 * The template for displaying search result page.
 */

get_header(); ?>

<div id="content" class="site-content container">
	<div class="row">
		<div id="primary" class="content-area col-md-9 col-sm-9 col-xs-12">
			<main id="main" class="site-main" role="main">
				<header class="page-header">
					<h1 class="page-title">Kết quả tìm kiếm cho: <?php echo esc_html( get_search_query() ) ?></h1>
				</header><!-- .page-header -->
				<?php onter_breadcrumbs() ?>
				<?php
				if (have_posts()) {
					onter_pagination();
					while (have_posts()) {
						the_post();
						get_template_part('template-parts/content/content', 'aside');
					}
					onter_pagination();
				}
				else {
					get_template_part('template-parts/content/content', 'none');
				}
				?>
			</main>
		</div>
		<div class="col-md-3 col-sm-3 col-xs-12">
			<?php get_sidebar() ?>
		</div>
	</div>
</div>

<?php get_footer(); ?>